<?php
defined('TYPO3_MODE') || die();

// Add static template
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile($_EXTKEY, 'Configuration/TypoScript/', 'Background image for content elements');