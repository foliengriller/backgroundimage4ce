<?php
namespace SBTheke\Backgroundimage4ce\Utility;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Sven Burkert <bedienung@sbtheke.de>, SBTheke web development
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

class FrontendUtility {

    /**
     * Add a style to the page, specific for this page
     *
     * The selector can be a contextual selector, like '#id .class p'
     * The presence of the selector is checked to avoid multiple entries of the
     * same selector.
     *
     * @param string $style
     * @return void
     */
    public static function addPageStyle($style) {
        if(!isset($GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_backgroundimage4ce.']['_CSS_PAGE_STYLE'])) {
            $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_backgroundimage4ce.']['_CSS_PAGE_STYLE'] = [];
        }
        $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_backgroundimage4ce.']['_CSS_PAGE_STYLE'][] = $style;
    }
}