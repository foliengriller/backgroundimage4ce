<?php
namespace SBTheke\Backgroundimage4ce\ViewHelpers;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2018 Sven Burkert <bedienung@sbtheke.de>, SBTheke web development
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractViewHelper;
use TYPO3\CMS\Core\Resource\FileRepository;
use TYPO3\CMS\Core\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use SBTheke\Backgroundimage4ce\Utility\FrontendUtility;

class RenderBackgroundViewHelper extends AbstractViewHelper {

    public function initializeArguments()
    {
        $this->registerArgument('cObj', 'array', 'Content Object');
    }

    /**
     * @param array $cObj: Content element
     * @return string
     */
    public function render() {
        $cObj = $this->arguments['cObj'];
        if($cObj['tx_backgroundimage4ce_active']) {
            $configuration = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_backgroundimage4ce.'];
            $declaration = [];

            // Generate class name
            if($configuration['classStdWrap.']) {
                $className = $GLOBALS['TSFE']->cObj->stdWrap($cObj['uid'], $configuration['classStdWrap.']);
            } else {
                $className = sprintf('backgroundimage4ce-c%s', $cObj['uid']);
            }

            // Get settings from content object
            if($cObj['tx_backgroundimage4ce_repeat']) {
                $declaration['background-repeat'] = $cObj['tx_backgroundimage4ce_repeat'];
            }
            if($cObj['tx_backgroundimage4ce_color']) {
                $declaration['background-color'] = $cObj['tx_backgroundimage4ce_color'];
            }
            if($cObj['tx_backgroundimage4ce_position']) {
                $declaration['background-position'] = $cObj['tx_backgroundimage4ce_position'];
            }
            if($cObj['tx_backgroundimage4ce_size']) {
                $declaration['background-size'] = $cObj['tx_backgroundimage4ce_size'];
            }
            if($cObj['tx_backgroundimage4ce_attachment']) {
                $declaration['background-attachment'] = $cObj['tx_backgroundimage4ce_attachment'];
            }

            // Process background image
            $uid = $cObj['uid']; // Content element uid
            $fileRepository = GeneralUtility::makeInstance(FileRepository::class);
            $fileReferences = $fileRepository->findByRelation('tt_content', 'tx_backgroundimage4ce', $uid);
            if(count($fileReferences)) {
                $fileReference = array_shift($fileReferences); // Get first image
                $fileObject = $fileReference->getOriginalFile();
                if($configuration['adaptiveImages'] && is_array($configuration['adaptiveImages.'])) {
                    $declarationMediaQueries = '';
                    foreach($configuration['adaptiveImages.'] as $v) {
                        list($processedFile, $processedFileHeight) = self::prepareImage($fileObject, $v['image']);
                        if($processedFile) {
                            // Calculate auto height
                            if($cObj['tx_backgroundimage4ce_autoheight'] && $processedFileHeight) {
                                $minHeight = $processedFileHeight;
                                if($configuration['padding']) {
                                    $minHeight -= (int)$configuration['padding'];
                                }
                            }
                            // Build media query
                            $declarationMediaQueries .= sprintf(
                                '@media %s {' . PHP_EOL . TAB . '.%s {' . PHP_EOL . TAB . TAB . 'background-image: url("%s");%s' . PHP_EOL . TAB . '}' . PHP_EOL . '}' . PHP_EOL,
                                $v['media'],
                                $className,
                                $processedFile,
                                $minHeight ? PHP_EOL . TAB . TAB . sprintf('min-height: %dpx;', $minHeight) : ''
                            );
                        }
                    }
                } else {
                    list($processedFile, $processedFileHeight) = self::prepareImage($fileObject, $configuration['image.']['max']);
                    if($processedFile) {
                        // Calculate auto height
                        if($cObj['tx_backgroundimage4ce_autoheight'] && $processedFileHeight) {
                            $minHeight = $processedFileHeight;
                            if($configuration['padding']) {
                                $minHeight -= (int)$configuration['padding'];
                            }
                            $declaration['min-height'] = sprintf('%dpx', $minHeight);
                        }
                        $declaration['background-image'] = sprintf(
                            'url("%s")',
                            $processedFile
                        );
                    }
                }
            }

            // Calculate manual height
            if(!$cObj['tx_backgroundimage4ce_autoheight'] && $cObj['tx_backgroundimage4ce_height']) {
                $minHeight = (int)$cObj['tx_backgroundimage4ce_height'];
                if($configuration['padding']) {
                    $minHeight -= (int)$configuration['padding'];
                }
                $declaration['min-height'] = sprintf('%dpx', $minHeight);
            }

            // Build CSS code
            $style = '';
            foreach($declaration as $k => $v) {
                $style .= sprintf(
                    TAB . '%s: %s;' . PHP_EOL,
                    $k,
                    $v
                );
            }
            $style = sprintf(
                '.%s {' . PHP_EOL . '%s}' . PHP_EOL,
                $className,
                $style
            );
            if(isset($declarationMediaQueries)) {
                $style .= $declarationMediaQueries;
            }
            FrontendUtility::addPageStyle($style);

            return $className;
        }
    }

    /**
     * Convert an image to a specific width and return absolute file path to processed image
     *
     * @param \TYPO3\CMS\Core\Resource\File $fileObject
     * @param integer $width
     * @return array: image path and image height
     */
    public static function prepareImage(\TYPO3\CMS\Core\Resource\File $fileObject, $width) {
        $processedFile = $fileObject->process(
            \TYPO3\CMS\Core\Resource\ProcessedFile::CONTEXT_IMAGECROPSCALEMASK,
            [
                'maxWidth' => (int)$width,
            ]
        );
        if($processedFile) {
            return [PathUtility::getAbsoluteWebPath($processedFile->getPublicUrl()), (int)$processedFile->getProperty('height')];
        }
        return FALSE;
    }

}